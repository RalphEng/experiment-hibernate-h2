Hibernate6 and H2 (Local)DateTime 
=================================

H2 Database does not store nano seconds (https://github.com/h2database/h2database/issues/1284) since version 1.4.198.

This make it hard for test. Because a simple DateTime save and load would not create an equal date time.

Hibernate5
----------

In Hibernate 5 a workarround was to register a custom Column Type for Timestamp in a custom Dialect:

```
public class CustomH2Dialect extends H2Dialect {
    public CustomH2Dialect() {
   		super();
   		registerColumnType(Types.TIMESTAMP, "timestamp(9)");
   	}
}
```
and register them as Hibernate dialect :

```
<persistence-unit ...>
   ...
   <properties>
      <property name="hibernate.dialect" value="...CustomH2Dialect"/>
   </properties>
<persistence-unit>
```

Hibernate6
----------

The Hibernate6 solution is also to use a custom dialect and override the method `getDefaultTimestampPrecision` method to return 9:

```
public class CustomH2Dialect extends H2Dialect {    

    @Override
    public int getDefaultTimestampPrecision() {
        return 9;
    }
}
```
