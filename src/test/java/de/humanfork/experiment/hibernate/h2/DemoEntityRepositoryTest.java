package de.humanfork.experiment.hibernate.h2;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@SpringBootTest
@Transactional
public class DemoEntityRepositoryTest {


    @Autowired
    private DemoEntityRepository demoEntityRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    void saveAndLoad() {

        //instaed of LocalDateTime.now() to have allways nanos 
        LocalDateTime withNannos = LocalDateTime.of(2024, 7, 7, 13, 31, 50, 435438800);

        DemoEntity demoEntity = new DemoEntity();
        demoEntity.setName("Test");        
        demoEntity.setTimestamp(withNannos);
        demoEntityRepository.save(demoEntity);

        entityManager.flush();
        entityManager.clear();

        entityManager.find(DemoEntity.class, demoEntity.getId());

        DemoEntity loaded = demoEntityRepository.findById(demoEntity.getId()).get();
        assertThat(loaded.getName()).isEqualTo(demoEntity.getName());
        assertThat(loaded.getTimestamp()).isEqualTo(demoEntity.getTimestamp());
    }

}
