package de.humanfork.experiment.hibernate;

import org.hibernate.dialect.H2Dialect;

public class CustomH2Dialect extends H2Dialect {

    /* The Hibernate 5 Solution was to call registerColumnType(Types.TIMESTAMP, "timestamp(9)"); in the ctor.*/ 
    @Override
    public int getDefaultTimestampPrecision() {
        return 9;
    }
}
