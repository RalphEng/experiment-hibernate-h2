package de.humanfork.experiment.hibernate.h2;

import java.time.LocalDate;
import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class DemoEntity {

    @Id 
    @GeneratedValue
    private Long id;

    private String name;

    //@Column(columnDefinition = "TIMESTAMP(9)")
    private LocalDateTime timestamp;
   
}
