package de.humanfork.experiment.hibernate.h2;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DemoEntityRepository extends JpaRepository<DemoEntity, Long>{

}
